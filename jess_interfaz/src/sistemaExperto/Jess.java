/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemaExperto;

import javax.swing.JPanel;
import jess.ConsolePanel;
import jess.Fact;
import jess.JessException;
import jess.Rete;
import jess.Value;
import net.sourceforge.jFuzzyLogic.FIS;

/**
 *
 * @author fLaVio
 */
public class Jess {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws JessException {
        
        
        //Cargamos el SE
        Rete r = new Rete();

        //Ruta del archivo CLP
        r.batch("src/sistemaExperto/SEDER.CLP");
        
        //Generamos el panel que entrega Jess para la interfaz gráfica
        ConsolePanel c = new ConsolePanel(r);
        //Enviamos el panel al Frame java que creamos
        interfazz Frame = new interfazz(c);
        Frame.add(c);
        Frame.setVisible(true);
        
        //reset para para cargar hechos, reglas, etc del CLP
        r.reset();
        //Ejecutamos el SE
        r.run();
        
        //System.out.println(presupuesto);
       
        
        //System.out.println(r.fetch("RES"));
        
        // Cargamos el archivo FCL con la descripcion del FIS
        String fileName = "src/moduloLD/Archivo-FCL.fcl";
        FIS fis2 = FIS.load(fileName, true);
        // Controlamos el no encontrar el archivo
        if (fis2 == null) {
            System.err.println("Can't load file: '"
                    + fileName + "'");
            return;
        }
        // graficar FIS
        fis2.chart();

        // Ingresar valores de entrada recuperados del SE
        fis2.setVariable("Edad_del_paciente", Double.parseDouble(r.fetch("Edad_del_paciente").toString()));
        fis2.setVariable("Cantidad_dias_presentando_sintomas", Double.parseDouble(r.fetch("Cantidad_dias_presentando_sintomas").toString()));

        //Evaluar FIS
        fis2.evaluate();

        // Generar grafico de salida
        fis2.getVariable("Vulnerabilidad_paciente_a_enfermedad").chartDefuzzifier(true);

        Double x = fis2.getVariable("Vulnerabilidad_paciente_a_enfermedad").getLatestDefuzzifiedValue();
        System.err.println("Para los valores de salida el grado de pertenencia es: " + x);
        
       Frame.jLabel5.setText((x).toString());
       
        
        // Print ruleSet
        System.out.println(fis2);
        

    }
}
